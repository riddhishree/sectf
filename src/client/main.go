package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"reflect"
	"runtime"
	"strings"
	"text/template"
	"time"

	"sectf.oauth.client/model"
)

var config = struct {
	home             string
	exchangeToken    string
	authURL          string
	login            string
	logout           string
	appID            string
	appPassword      string
	callback         string
	tokenEndpoint    string
	servicesEndpoint string
}{
	home:             "http://localhost:8000/",
	exchangeToken:    "http://localhost:8000/exchangeToken",
	authURL:          "http://localhost:8080/auth/realms/SeCTF/protocol/openid-connect/auth",
	login:            "http://localhost:8000/login",
	logout:           "http://localhost:8080/auth/realms/SeCTF/protocol/openid-connect/logout",
	appID:            "mirage",
	appPassword:      "84b1a5fd-8768-48d0-9031-672b83198371",
	callback:         "http://localhost:8000/sectf",
	tokenEndpoint:    "http://localhost:8080/auth/realms/SeCTF/protocol/openid-connect/token",
	servicesEndpoint: "http://localhost:8001/billing/v1/services",
}

var t = template.Must(template.ParseFiles("template/index.html", "template/login.html"))
var tHome = template.Must(template.ParseFiles("template/index.html", "template/home.html"))
var tServices = template.Must(template.ParseFiles("template/index.html", "template/services.html"))

//SessionVar variable type will capture OAUTH2.0 session details
type SessionVar struct {
	AuthCode     string
	SessionState string
	AccessToken  string
	Scope        string
	RefreshToken string
	Services     []string

	state        string
	code         string
	redirectURL  string
	clientSecret string
	accessToken  string
	clickjacking string
}

var sessionVar = SessionVar{}

func init() {
	log.SetFlags(log.Ltime)
}

func main() {
	fmt.Println("hello")
	http.HandleFunc("/", enabledLog(home))
	http.HandleFunc("/login", enabledLog(login))
	http.HandleFunc("/exchangeToken", enabledLog(exchangeToken))
	http.HandleFunc("/sectf", enabledLog(sectf))
	http.HandleFunc("/services", enabledLog(services))

	http.HandleFunc("/state", enabledLog(state))
	http.HandleFunc("/code", enabledLog(code))
	http.HandleFunc("/redirectURL", enabledLog(redirectURL))
	http.HandleFunc("/clientSecret", enabledLog(clientSecret))
	http.HandleFunc("/accessToken", enabledLog(accessToken))
	http.HandleFunc("/clickjacking", enabledLog(clickjacking))

	http.HandleFunc("/logout", enabledLog(logout))
	http.ListenAndServe(":8000", nil)
}

func enabledLog(handler func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {

	return func(w http.ResponseWriter, r *http.Request) {
		handlerName := runtime.FuncForPC(reflect.ValueOf(handler).Pointer()).Name()
		log.SetPrefix(handlerName + " ")
		log.Println("-->" + handlerName)
		log.Printf("request : %+v\n", r.RequestURI)
		// log.Printf("response : %+v\n", w)
		handler(w, r)
		log.Println("<--" + handlerName + "\n")
	}
}

func home(w http.ResponseWriter, r *http.Request) {
	t.Execute(w, sessionVar)
}

func exchangeToken(w http.ResponseWriter, r *http.Request) {
	form := url.Values{}
	form.Add("grant_type", "authorization_code")
	form.Add("code", sessionVar.AuthCode)
	form.Add("redirect_uri", config.callback)
	form.Add("client_id", config.appID)

	req, err := http.NewRequest("POST", config.tokenEndpoint, strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if err != nil {
		log.Println(err)
		return
	}

	req.SetBasicAuth(config.appID, config.appPassword)

	//Client
	c := http.Client{}
	res, err := c.Do(req)
	if err != nil {
		log.Printf("Access token could not be retrieved", err)
		return
	}

	byteBody, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()

	if err != nil {
		log.Println(err)
		return
	}

	accessTokenResponse := &model.AccessTokenResponse{}
	json.Unmarshal(byteBody, accessTokenResponse)

	sessionVar.AccessToken = accessTokenResponse.AccessToken
	sessionVar.Scope = accessTokenResponse.Scope
	sessionVar.RefreshToken = accessTokenResponse.RefreshToken

	tHome.Execute(w, sessionVar)
}

func login(w http.ResponseWriter, r *http.Request) {
	//Creating redirect URL for authentication endpoint
	req, err := http.NewRequest("GET", config.authURL, nil)
	if err != nil {
		log.Print(err)
	}

	qs := url.Values{}
	qs.Add("state", "SeCTF")
	qs.Add("client_id", config.appID)
	qs.Add("response_type", "code")
	qs.Add("redirect_uri", config.callback)

	req.URL.RawQuery = qs.Encode()

	http.Redirect(w, r, req.URL.String(), http.StatusFound)
}

func sectf(w http.ResponseWriter, r *http.Request) {
	sessionVar.AuthCode = r.URL.Query().Get("code")
	sessionVar.SessionState = r.URL.Query().Get("session_state")
	fmt.Printf("Request queries : %+v\n", sessionVar)
	r.URL.RawQuery = ""

	http.Redirect(w, r, config.exchangeToken, http.StatusFound)
}

func logout(w http.ResponseWriter, r *http.Request) {

	logoutURL, err := url.Parse(config.logout)
	if err != nil {
		log.Println(err)
	}

	qs := url.Values{}
	qs.Add("redirect_uri", config.home)

	logoutURL.RawQuery = qs.Encode()

	http.Redirect(w, r, logoutURL.String(), http.StatusFound)
}

func services(w http.ResponseWriter, r *http.Request) {

	//request
	req, err := http.NewRequest("GET", config.servicesEndpoint, nil)
	if err != nil {
		log.Println(err)
		return
	}

	req.Header.Add("Authorization", "Bearer "+sessionVar.AccessToken)

	//client
	ctx, cancelFunc := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancelFunc()

	c := http.Client{}
	res, err := c.Do(req.WithContext(ctx)) //fail if wait is smore than 1 second
	if err != nil {
		log.Println(err)
		tServices.Execute(w, sessionVar)
		return
	}

	byteBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		tServices.Execute(w, sessionVar)
		return
	}

	//process response
	billingResponse := &model.BillingResponse{}
	err = json.Unmarshal(byteBody, billingResponse)
	if err != nil {
		log.Println(err)
		tServices.Execute(w, sessionVar)
		return
	}
	sessionVar.Services = billingResponse.Services

	tServices.Execute(w, sessionVar)
}

// The state parameter was introduced to mitigate CSRF attacks.
func state(w http.ResponseWriter, r *http.Request) {
	//request
	req, err := http.NewRequest("GET", config.servicesEndpoint, nil)
	if err != nil {
		log.Println(err)
		return
	}

	req.Header.Add("Authorization", "Bearer "+sessionVar.AccessToken)

	//client
	ctx, cancelFunc := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancelFunc()

	c := http.Client{}
	res, err := c.Do(req.WithContext(ctx)) //fail if wait is smore than 1 second
	if err != nil {
		log.Println(err)
		tServices.Execute(w, sessionVar)
		return
	}

	byteBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		tServices.Execute(w, sessionVar)
		return
	}

	//process response
	billingResponse := &model.BillingResponse{}
	err = json.Unmarshal(byteBody, billingResponse)
	if err != nil {
		log.Println(err)
		tServices.Execute(w, sessionVar)
		return
	}
	sessionVar.Services = billingResponse.Services

	tServices.Execute(w, sessionVar)
}

// The code parameter contains the authorization code received from the authorization server by the Resource Owner.
func code(w http.ResponseWriter, r *http.Request) {

}

// The authorization server redirects the user-agent to the client’s redirection endpoint previously established with the authorization server during the client registration process or when making the authorization request”
func redirectURL(w http.ResponseWriter, r *http.Request) {

}

// In the OAuth 2.0 specification of the Authorization Code grant, the client_secret is a value unique to every resource provider, that allows it to exchange valid codes (with state, if required) for access tokens.
func clientSecret(w http.ResponseWriter, r *http.Request) {

}

// In the Authorization Code grant flow, the access token should never be exposed to the client, only used by the resource provider.
func accessToken(w http.ResponseWriter, r *http.Request) {

}

// The “X-Frame-Options” header is the legacy mitigation for clickjacking, while the Content Security Policy frame-ancestors directive is a more modern control.
func clickjacking(w http.ResponseWriter, r *http.Request) {

}
