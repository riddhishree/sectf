package main

import (
	// "fmt"

	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"reflect"
	"runtime"
	"strings"

	"sectf.oauth.billing/model"
)

//Billing list of services to pay
type Billing struct {
	Services []string `json:"services"`
}

//BillingError error response
type BillingError struct {
	Error string `json:"error"`
}

//TokenINtrospect response
type TokenIntrospect struct {
	Exp    int    `json:"exp"`
	Nbf    int    `json:"nbf"`
	Iat    int    `json:"iat"`
	Jti    string `json:"jti"`
	Aud    string `json:"aud"`
	Typ    string `json:"typ"`
	Acr    string `json:"acr"`
	Active bool   `json:"active"`
}

var config = struct {
	tokenIntrospection string
}{
	tokenIntrospection: "http://localhost:8080/auth/realms/SeCTF/protocol/openid-connect/token/introspect",
}

func main() {
	http.HandleFunc("/billing/v1/services", enabledLog(services))
	http.ListenAndServe(":8001", nil)
}

func enabledLog(handler func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {

	return func(w http.ResponseWriter, r *http.Request) {
		handlerName := runtime.FuncForPC(reflect.ValueOf(handler).Pointer()).Name()
		log.SetPrefix(handlerName + " ")
		log.Println("-->")
		log.Printf("request : %+v\n", r.RequestURI)
		//log.Printf("response : %+v\n", w)
		handler(w, r)
		log.Println("<--")
	}
}

func services(w http.ResponseWriter, r *http.Request) {

	token, err := getToken(r)
	if err != nil {
		log.Println(err)
		showErrorMessage(w, err.Error())
		return
	}

	// Validate token
	if !validateToken(token) {
		showErrorMessage(w, err.Error())
		return
	}

	claimBytes, err := getClaim(token)
	if err != nil {
		log.Println(err)
		showErrorMessage(w, "Cannot parse token claim")
		return
	}
	tokenClaim := &model.Tokenclaim{}
	err = json.Unmarshal(claimBytes, tokenClaim)
	if err != nil {
		log.Println(err)
		showErrorMessage(w, err.Error())
		return
	}

	// scopes := strings.Split(tokenClaim.Scope, " ")
	// for _, v := range scopes {
	// 	log.Println("Scope : ", v)
	// }
	if !strings.Contains(tokenClaim.Scope, "getBillingService") {
		showErrorMessage(w, "Invalid token scope. required scope [getBillingService]")
		return
	}

	s := Billing{
		Services: []string{
			"electric",
			"phone",
			"internet",
			"water",
		},
	}
	encoder := json.NewEncoder(w)
	w.Header().Add("Content Type", "application/json")
	encoder.Encode(s)
	// fmt.Fprintf(w, "%v", s)
}

func getToken(r *http.Request) (string, error) {
	//header
	token := r.Header.Get("Authorization")
	if token != "" {
		auths := strings.Split(token, " ")
		if len(auths) != 2 {
			return "", fmt.Errorf("Invalid Authorization header format")
		}
		return auths[1], nil
	}

	//form body
	token = r.FormValue("access_token")
	if token != "" {
		return token, nil
	}

	//query
	token = r.URL.Query().Get("access_token")
	if token != "" {
		return token, nil
	}

	return token, fmt.Errorf("Missing access token")
}

func validateToken(token string) bool {
	//Request
	form := url.Values{}
	form.Add("token", token)
	form.Add("token_type_hint", "requesting_party_token")
	req, err := http.NewRequest("POST", config.tokenIntrospection, strings.NewReader(form.Encode()))
	if err != nil {
		log.Println(err)
		return false
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth("tokenChecker", "7e92580f-e5dd-44d6-8da5-753a0ee20e32")

	//Client
	c := http.Client{}
	res, err := c.Do(req)
	if err != nil {
		log.Println(err)
		return false
	}

	//Process response
	if res.StatusCode != 200 {
		log.Println("Status is not 200 : ", res.StatusCode)
		return false
	}

	byteBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		return false
	}
	defer res.Body.Close()
	introspect := &TokenIntrospect{}
	err = json.Unmarshal(byteBody, introspect)
	if err != nil {
		log.Println(err)
		return false
	}

	return introspect.Active
}

func showErrorMessage(w http.ResponseWriter, errMsg string) {
	s := &BillingError{Error: errMsg}
	encoder := json.NewEncoder(w)
	w.Header().Add("Content Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	encoder.Encode(s)
}

func getClaim(token string) ([]byte, error) {
	tokenParts := strings.Split(token, ".")
	claim, err := base64.RawURLEncoding.DecodeString(tokenParts[1])
	if err != nil {
		return []byte{}, err
	}
	return claim, nil
}
